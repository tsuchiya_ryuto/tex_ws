\chapter{走行コストの予測手法}\label{chap:proposed_method}

自己教師あり学習とは，人が与えた教師データではなくロボット自身が収集した教師データを利用することで教師あり学習を行うことである．本研究では，移動ロボットにRGB-Dカメラと加速度センサを搭載し，カメラで撮影した地形画像とその地形の振動データを教師データとして収集する．さらに，収集した教師データを利用し，畳み込みニューラルネットワークを用いて予測器を学習する．

本研究では，画像と振動情報を教師データとして利用するが，地形画像の撮影とその地形の振動情報の取得には時間的な遅れが生じる．そのため，教師データを作成する際，時間的な遅れを考慮したデータの対応付けを行う必要がある．
最も簡単な解決方法としては，画像と加速度の取得時に得たタイムスタンプを利用する方法である．ロボットが等速走行していると仮定すれば，遅れは常に一定であるため，カメラの画角とロボットの走行速度を考慮し，遅れを算出することで，画像と振動情報の対応付けが可能である．
しかし，不整地ではロボットは様々な要因により，一定の速度を保つことは難しい．さらに，移動ロボットの導入を考慮した際，ロボットはある程度速い速度での走行が可能な方が好ましいが，速度が速いほど撮影した画像のブレは大きくなると考えられ，効率的な学習が困難となる．
そこで，本研究では地形画像の撮影と加速度の取得を分けて走行させることで，それぞれに適した速度で走行をさせることを可能にした．

タイムスタンプによる画像と振動情報の対応付けは，画像撮影と加速度取得を同時に行うことを前提としている方法であるため，走行を分けて行う場合，別のデータの対応付け手法を考える必要がある．そこで，本研究ではvisualSLAMを用いた自己位置推定を行い，位置情報によるデータの対応付け手法を提案した．

\section{画像と振動情報の対応付け}\label{sec:data-matching}

本研究では，ROS\cite{ros}パッケージのRTABMAP\cite{rtabmapros}というvisualSLAMを利用して自己位置を推定する．RTABMAPではRGB-Dカメラにより取得した色情報と深度情報から3次元点群を生成し，推定した自己位置情報により最適化することで環境地図を構築する．また，作成した環境地図と，現在の色・深度情報を照らし合わせることで，現在位置の推定を行うことが可能である．

本研究では，1つの環境に対して以下のように合計3回の走行が必要である．

\begin{enumerate}
	\item RTABMAPにより実験を行いたい環境の地図を作成する．\label{run1}
	\item 地形画像取得のため作成した環境地図上で，自己位置推定をしながらロボットを走行させる．\label{run2}
	\item 加速度取得のため作成した環境地図上で，自己位置推定をしながらロボットを走行させる．\label{run3}
\end{enumerate}

\ref{run1}回目の走行で作成した環境地図の例を図\ref{fig:map}に示す．図\ref{fig:map}の青線は\ref{run1}回目の走行で地図を作製した際の移動軌跡であり，赤線の経路を2回目と3回目で走行させ，画像撮影と加速度取得を行う．このとき，画像撮影時は地形の粗さによる振動により生じる画像のブレを抑制するために低速で走行させる．
\ref{run2}回目の走行で取得した連続画像と位置情報はタイムスタンプが一致しているため，図\ref{fig:image-position}のように時間軸情報により簡単に画像と位置情報の対応付けを行うことが可能である．同様に，\ref{run3}回目の走行で取得した加速度と位置情報もタイムスタンプが一致しているため，図\ref{fig:acc-position}のように加速度と位置情報の対応付けを行う．ここで，対応付けする加速度は加速度領域として与えられ，ある地形を走行している間の時間領域の加速度信号を加速度領域とする．時間領域は，\ref{subsec:image_cut}節で説明する画像の切り取り範囲によって決定する．

\begin{figure}[b]
	\begin{center}
		\includegraphics[width=\linewidth]{fig/chapter3/map.eps}
		\caption{RTABMAPにより作成した環境地図の例}
		\label{fig:map}
	\end{center}
\end{figure}

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=\linewidth]{fig/chapter3/image-position.eps}
		\caption{画像と位置情報の対応付け}
		\label{fig:image-position}
	\end{center}
\end{figure}

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=\linewidth]{fig/chapter3/acc-position.eps}
		\caption{加速度と位置情報の対応付け}
		\label{fig:acc-position}
	\end{center}
\end{figure}

\clearpage

ある地形を表現する画像と加速度は，図\ref{fig:delay}のように位置のずれがある．ここで，画像$I$を撮影した位置を$P_I$，画像$I$の地形を走行した時の位置を$P_A$とし，そのずれ幅を$d$としたすると，画像と加速度の対応付けは式\ref{eq:position-matching}が成立するように行うことで，画像と振動情報の対応付けが可能となる．

\begin{equation}\label{eq:position-matching}
P_A = P_I + d
\end{equation}

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=\linewidth]{fig/chapter3/delay.eps}
		\caption{データ取得時の位置関係}
		\label{fig:delay}
	\end{center}
\end{figure}

\clearpage


\section{入力画像の前処理}

効率良く学習を行うには，撮影した画像をそのまま利用するのではなく，様々な前処理をする必要がある．
本節では，入力画像の前処理について説明する．

\subsection{画像の切り取り}\label{subsec:image_cut}
移動ロボットに搭載したカメラは，SLAMのセンサとしても利用するため地面のみを撮影するのではなく，ロボットの周囲を見渡せるように設置する必要がある．そのため，撮影した画像には地形の危険性予測に必要のない情報を多く含んでいる．そこで，撮影した画像の中から走行予定の地形領域のみを切り取る必要がある．

元画像を図\ref{fig:original}に示す．切り取り前の元画像は$640 \times 480$の画像サイズである．横幅がロボットの両車輪より大きく，縦幅は地面のみが映るように切り取り範囲を決定する．また，学習にはRGB-Dカメラで撮影した深度画像も利用するが，深度情報は距離が遠いほど精度が落ちるため，縦幅は画像の下端から0.5mとなるように切り取りを行う．以上を考慮して，横$100 \sim 540$pixel，縦$330 \sim 480$pixelの領域を走行予定の地形領域とした．（図\ref{fig:cut}）ここで切り取りした$440 \times 150$の画像の左右の領域のみが，車輪が通過する領域であるため，画像中央の地形は振動に影響しない．そのため，$440 \times 150$の画像の両端から180pixelの領域を車輪の走行予定地形として切り取りを行う．さらに，切り取り後の$180 \times 150$の画像を$150 \times 150$の正方形の画像にリサイズして学習を行う．深度画像についても同様の処理を行う．最終的な地形画像を図\ref{fig:InputImage}に示す．

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=\linewidth]{fig/chapter3/original.png}
		\caption{元画像(画像サイズ：$640 \times 480$)}
		\label{fig:original}
	\end{center}
\end{figure}

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=8cm]{fig/chapter3/cut.png}
		\caption{切り取り後(画像サイズ：$440 \times 150$)}
		\label{fig:cut}
	\end{center}
\end{figure}

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=\linewidth]{fig/chapter3/input.eps}
		\caption{入力画像}
		\label{fig:InputImage}
	\end{center}
\end{figure}

\clearpage

\subsection{画像の正規化}
次に画像の正規化を行う．撮影された画像は，カラー画像が8バイト，深度画像が16バイトの情報であるため，学習前にそれらを規格化する必要がある．
全ての画素に対して，式\ref{eq:normalize}により正規化を行うことで，最大値1，最小値0に画素を規格化する．ただし，$x_{max}$，$x_{min}$は各チャンネルの最大，最小である．例えば，深度情報であれば，$x_{max}$は全ての深度情報の中で最大の画素値である．

\begin{equation}\label{eq:normalize}
x^{\prime} = \frac{x - x_{min}}{x_{max} - x_{min}}
\end{equation}

\subsection{ブレ画像の除去}
最後に，ブレ画像の除去を行う．一般に，画像はブレが大きいほどエッジを検出することが難しい．そのため，エッジの量を算出できれば，画像のブレを定量的に評価できる．そこで，画像に8近傍のラプラシアンフィルタによりエッジ検出を行い，検出されたエッジの量として全画素値の分散を式\ref{eq:blur_evaluation}により計算することで評価を行う．分散値が閾値以下の画像をブレ画像として除去する．

\begin{equation}\label{eq:blur_evaluation}
\sigma = \sum_i\sum_j(x_{i,j} - \bar{x})^2
\end{equation}

\section{走行コストの定義}\label{sec:definition_cost}

走行コストは振動情報の指標として，教師あり学習の出力とする．そのため，加速度情報から適切に振動情報を抽出する必要がある．
ロボットが段差を昇るとき，ロボットには地面と垂直方向に抵抗力が発生する．それにより，地面と垂直方向の加速度は抵抗力により変化する．そのため，加速度の変化は衝撃値の指標として扱うことが可能である．そのため，本実験では\ref{sec:data-matching}節により対応付けされた加速度領域で，画像$I_j$に対応する走行コスト$C_j$は，式\ref{eq:cost_define}により算出する．ここで，$K_j$は対応付けされた加速度の時間領域であり，移動ロボットが0.5mだけ進む時間と加速度センサのサンプリングレートによって決定する．また，走行コストは左右それぞれについて算出するため2次元の値である．

\begin{equation}\label{eq:cost_define}
C_j = \frac{1}{\left| K_j \right|}\sum_{k \in K_j}(a_{k+1} - a_{k})^2
\end{equation}

実際に図\ref{fig:acceleration_signal}に示す加速度信号を式\ref{eq:cost_define}を用いて算出した走行コストを図\ref{fig:cost_example}に示す．
図\ref{fig:cost_example}のように，振動特徴が抽出出来ていることが確認できる．

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=\linewidth]{fig/chapter3/acceleration_signal.eps}
		\caption{加速度信号の例}
		\label{fig:acceleration_signal}
	\end{center}
\end{figure}

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=\linewidth]{fig/chapter3/cost_example.eps}
		\caption{図\ref{fig:acceleration_signal}から算出した走行コスト}
		\label{fig:cost_example}
	\end{center}
\end{figure}

\section{畳み込みニューラルネットワーク}
本研究では，走行コストの予測手法として画像処理に一般的に用いられるニューラルネットワークである畳み込みニューラルネットワーク(CNN)を用いる．
入力は左右の色・深度画像であり，1組のデータセットに4枚の画像が存在する．出力は，\ref{sec:definition_cost}節で定義した2次元の走行コストである．図\ref{fig:cnn}に示すような4入力2出力のCNNのモデルを設計し，学習を行う．移動速度変化を考慮する際は，速度の入力を加えるため，図\ref{fig:cnn}のように5入力2出力のモデルとなる．また，畳み込み層の構成を表\ref{tab:cnn}に示す．なお，活性化関数はReLUを用いた．

\begin{figure}[tbp]
	\begin{center}
		\includegraphics[width=\linewidth]{fig/chapter3/cnn.eps}
		\caption{CNNの層構成}
		\label{fig:cnn}
	\end{center}
\end{figure}

\begin{table}[htpb]
 \caption{畳込み層の詳細}
 \label{tab:cnn}
 \centering
 \footnotesize
 \begin{tabular}{|c|c|c|}
  \hline
  層の処理  &フィルタサイズ & チャネル数 \\\hline
  畳込み　&$11 \times 11$ &16 \\\hline
  最大プーリング　&$3 \times 3$ &- \\\hline
  畳込み　&$5 \times 5$ &32 \\\hline
  最大プーリング　&$2 \times 2$ &- \\\hline
	畳込み　&$3 \times 3$ &64 \\\hline
  最大プーリング　&$2 \times 2$ &- \\\hline
  %\hline
 \end{tabular}
\end{table}



