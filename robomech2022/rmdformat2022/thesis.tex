\documentclass{jarticle}
\usepackage{robomech2022}
%\usepackage{graphicx}
\usepackage[dvipdfmx]{graphicx}
\usepackage[hang,small,bf]{caption}
\usepackage[subrefformat=parens]{subcaption}
\captionsetup{compatibility=false}

\begin{document}
\makeatletter
\title{不整地環境における移動ロボットの自己教師あり学習を用いた\\地形危険性予測手法の検討}
{}
{Prediction of Terrain Traversability Cost Using Self-supervised Learning for Mobile Robot in Uneven Terrain}
{}

\author{
\begin{tabular}{ll}
 \hspace{1zw}○\hspace{1zw}土屋 竜人 (静岡大)& 正\hspace{1zw} 小林 祐一（静岡大）\\
 %\hspace{1zw}学\hspace{1zw}東京　学（西大）& [日本語著者名：明朝体10pt]\\
 % ※協賛・後援団体の会員資格で発表される場合は「正・学」は不要です。
 &\\
 \multicolumn{2}{l}{\small Ryuto TSUCHIYA, Shizuoka University, tsuchiya.ryuto.18@shizuoka.ac.jp}\\
 \multicolumn{2}{l}{\small Yuichi KOBAYASHI, Shizuoka University}\\
\end{tabular}
}
\makeatother

\abstract{ \small 
In this paper, we propose a method to predict terrain traversability cost using self-supervised learning for mobile robot in uneven terrain. The robot learns to predict risk of traversing the terrain by using the vibration information from the acceleration sensor as the prediction target and the color and depth image from the camera as the input. We conducted an experiment to get the datasets in a real environment and predict terrain traversability cost from terrain image at constant speed. The prediction results showed that low and medium cost could be predicted. In addition, we propose safer prediction method considering speed change.
}

\date{} % 日付を出力しない
\keywords{Outdoor mobile robot, Self-supervised learning, Uneven terrain}

\maketitle
\thispagestyle{empty}
\pagestyle{empty}

\small
\section{緒言}%===========================
近年，農業現場や工場での人手不足の対策，災害現場や宇宙環境などの危険な環境での作業などで人の代替として移動ロボットの導入が広く検討されている．これらのロボットには自律的に周囲状況を認識・判断をすることが要求されるが，現在多くのロボットは導入前に事前整備が必要である．しかし，事前整備にはコストや時間を要するため，これらが導入時の大きな障害となっている．
そこで，非整備環境での移動ロボットの自律走行が求められている．

移動ロボットの自律走行では，SLAM(Simultaneous Localization and Mapping)\cite{ProbabilityRobotics}などを用いて，環境地図を作成することが一般的である．しかし，SLAMではある程度の高さがある物体しか障害物として認識しないため，地面の粗さや石などの背の低い障害物を認識することは難しく，不整地環境では適用が困難である．不整地環境には，石や木の根などの段差による振動や砂地などの滑り，瓦礫の隙間に嵌って身動きが取れなくなるなど，様々な危険性が存在する．不整地環境で移動ロボットの自律走行を行うためには，このような危険性を予測する環境認識手法が必要である．本研究では特に振動による危険性について取り扱う．

Valada \textit{et al.} は非整備の山道などで深層学習を用いた地形の分類による走行可能性の判断を行っている \cite{Valada2016}．しかし，地形分類手法では大量の画像を画素ごとに手作業でラベル付けを行う必要があり，多くの手間と時間を要する．そこで，Brooks \textit{et al.} は手作業で危険性を与えるのではなく，ロボット自身に地形の危険性を判断させる自己教師あり学習を提案した \cite{Brooks2012}．自己教師あり学習では，外界センサ（カメラ,Lidarなど）で取得した環境情報と内界センサ（加速度センサなど）で取得した振動などの危険性を教師データとして教師あり学習を行う．これにより，人が手作業で危険性を与えなくても，地形の危険性を判断できる．Bekhti \textit{et al.} はロボットにカメラと加速度センサを搭載し，地形画像の特徴量と加速度から算出した衝撃値をデータセットとしてガウス過程回帰により自己教師あり学習を行い，地形危険性の予測が可能であることを示した \cite{Bekhti2020}．しかし，この手法では撮影した地形を実際にロボットに走行させる必要があるため，危険地形の予測にはロボットに危険を経験させる必要がある．

自己教師あり学習による予測手法は，従来手法と比べ手作業を大幅に削減可能なだけでなく，実際にロボットが経験した危険性を評価指標としているため，ロボットにより適切な危険性を予測可能である．しかし，危険地形の予測を行うとき，教師データ収集のためにロボットに危険地形を走行させる必要がある．そこで，本研究では安全性を考慮した自己教師あり学習による地形危険性の予測を行うことを目的とする．
移動により生じる衝撃は移動速度に大きく依存するため，移動速度の変化を考慮した予測を行うことで，データ収集の際にロボットに生じる衝撃を最小限に低減する予測手法について検討する．
本稿では，その第一段階として移動速度一定での地形危険性予測を行い，その後移動速度を考慮した予測手法について検討する．

%===========================

\section{問題設定}

本研究は，図\ref{fig: original}のような地面の整地がされておらず，地形の粗さにより振動が生じる不整地環境を想定している．また，路面にはロボットが踏破不可能な高さの障害物はなく，傾斜や滑りが小さい地形で実験を行う．
移動ロボットにはRGB-Dカメラと加速度センサを搭載する．RGB-Dカメラでは，地形の見た目の情報だけでなく，深度情報を学習に組み込むことで3次元情報も利用し，色・深度情報を合わせた画像を地形画像とする．また，加速度センサは走行時の振動情報を危険性として定量評価するために用いられる．さらに，地形画像と振動情報を対応付けるために自己位置推定を行う必要があり，カメラを用いて地図生成と自己位置推定を同時に行うvisualSLAM\cite{rtabmap}を用いる．
図\ref{fig: problem}に示すようにカメラで撮影した地形をロボットに走行させ，その時の加速度を計測することで地形画像と振動情報を取得し，自己位置情報により対応付けを行う．この地形画像と振動情報から深層学習の回帰モデルを学習させることで，最終的に地形画像から振動情報の予測を行うことを目的とする．


\begin{figure}[tbp]
	\begin{center}
		\includegraphics[width=\linewidth]{fig/proposed_method.eps}
		\caption{Self-supervised learning for predicting traversability cost}
		\label{fig: problem}
	\end{center}
\end{figure}


%===========================

\section{地形危険性の予測手法}

\subsection{入力画像の前処理}

撮影した画像は地形の危険性予測に必要のない情報を多く含んでいる．そこで，撮影した画像の中から走行予定の地形領域のみを切り取る必要がある．そのため，横幅は車輪幅より大きく，また，縦幅は画像の下端から0.5mとなるように切り取りを行う．元画像と切り取り後の画像をそれぞれ図\ref{fig: original}，図\ref{fig: cut}に示す．
また，切り取り後の画像の中央の領域は，車輪の通過しない領域であるため，図\ref{fig: cut}の赤枠の領域のみを学習に使用する．
同様の処理を深度画像にも行い，得られた左右の色・深度画像を地形画像とする．

次に，画像の正規化を行う．撮影された画像は，カラー画像が8ビット，深度画像が16ビットの情報であるため，学習前に全ての画素に対して最大値1，最小値0となるように規格化する．
最後に，ブレ画像の除去を行う．一般に，画像はブレが大きいほどエッジを検出することが難しい．そこで，地形画像にエッジ検出を行い，全画素値の分散を画像のブレとして評価する．分散値が閾値以下の画像をブレ画像として除去する．

\begin{figure}[tb]
	\begin{center}
		\includegraphics[height=40mm]{fig/original.png}
		\caption{original image}
		\label{fig: original}
	\end{center}
\end{figure}

\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=40mm]{fig/terrain_image.eps}
		\caption{image after cropping}
		\label{fig: cut}
	\end{center}
\end{figure}

%------------------------------

\subsection{走行コストの定義}

走行コストは振動情報の指標として，教師あり学習の出力とする．画像$I_j$に対応する走行コスト$C_j$は，式(\ref{eq: cost_define})により算出する．ここで，$K_j$は移動ロボットが0.5m進む間の加速度の時間領域である．なお，単位は加速度の2乗となるため，$[\rm{(m/s^2)^2}]$である．
%また，走行コストは左右は2次元の値である．
\begin{equation}\label{eq: cost_define}
C_j = \frac{1}{\left| K_j \right|}\sum_{k \in K_j}(a_{k+1} - a_{k})^2
\end{equation}

%------------------------------

\subsection{地形画像と走行コストの対応付け}

地形画像に対応する加速度は，カメラにより撮影した地形を実際にロボットが走行することで取得することができる．地形画像と加速度信号には取得タイミングにずれが生じるため，ずれを考慮して対応付けを行う必要がある．そこで，本研究では画像撮影と加速度取得の際に，同時に自己位置推定を行うことで，自己位置情報を用いた地形画像と走行コストの対応付けを行う．自己位置推定には，RTABMAP\cite{rtabmap}というvisualSLAMを使用する．
本手法では1つの環境に対して以下のような3回の走行を行う．

\begin{enumerate}
	\item visualSLAMを用いた環境地図の作成\label{run1}
	\item 作成した環境地図上での画像撮影と自己位置推定\label{run2}
	\item 作成した環境地図上での加速度取得と自己位置推定\label{run3}
\end{enumerate}

図\ref{fig: map}は，実際に作成した地図である．図の青線は1度目の走行で地図を作成した際の移動軌跡であり，2度目と3度目の走行では赤線の経路を走行させ，画像撮影と加速度取得を行う．このとき，画像撮影時は画像のブレを抑制するために低速走行させる．
3度目の走行で得られた加速度と自己位置情報は図\ref{fig: matching}上図のように，タイムスタンプが一致している．そのため，時刻がおおよそ等しくなるように対応付けを行う．このように対応付けした時刻を始点として，ロボットが0.5m進む間の時間領域を$K_j$とする．得られた時間領域$K_j$の加速度から式(\ref{eq: cost_define})により走行コストを算出したものが，図\ref{fig: matching}下図である．
画像と自己位置情報も同様の手順で時刻を一致させることで対応付けを行う．最後に，画像と走行コストを位置のずれを考慮して位置情報により対応付けを行う．

\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=80mm]{fig/map.eps}
		\caption{Map created by using RTABMAP}
		\label{fig: map}
	\end{center}
\end{figure}

\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=62mm]{fig/matching.pdf}
		\caption{Matching between acceleration and localization pose}
		\label{fig: matching}
	\end{center}
\end{figure}

%------------------------------

\subsection{CNNの層構成}
本研究では入力を地形画像，出力を走行コストとした教師データを用いてCNNによる予測器の学習を行う．ここで，地形画像は左右の色・深度画像であり，1つのデータセットに4枚の画像が存在する．また，出力の走行コストは左右の走行コストであるため，2次元である．そのため，図\ref{fig: cnn}に示すような4入力2出力のCNNのモデルを設計した．移動速度変化を考慮する際は，さらに速度の入力を加えるため5入力2出力のモデルとなる．
また，畳み込み層の構成を表\ref{tab: cnn}に示す．
なお，活性化関数はReLUを用いる．

\begin{figure}[tbp]
	\begin{center}
		\includegraphics[width=90mm]{fig/cnn.eps}
		\caption{Network to predict traversability cost}
		\label{fig: cnn}
	\end{center}
\end{figure}

\begin{table}[htpb]
 \caption{Detail of convolutional layers}
 \label{tab: cnn}
 \centering
 \footnotesize
 \begin{tabular}{|c|c|c|}
  \hline
  層の処理  &フィルタサイズ & チャネル数 \\\hline
  畳込み　&$11 \times 11$ &16 \\\hline
  最大プーリング　&$3 \times 3$ &- \\\hline
  畳込み　&$5 \times 5$ &32 \\\hline
  最大プーリング　&$2 \times 2$ &- \\\hline
	畳込み　&$3 \times 3$ &64 \\\hline
  最大プーリング　&$2 \times 2$ &- \\\hline
  %\hline
 \end{tabular}
\end{table}

%===========================

\section{実験}

\subsection{条件}

実験で使用した移動ロボットは，4輪ロボットのPioneer3ATである．RGB-DカメラとしてIntel RealSense D435，加速度センサとして9軸IMUセンサモジュールを移動ロボットに左右1つずつ搭載した．
実験を行った環境は，砂利道(図\ref{fig: gravel}）や小石や木の根が散乱した荒れ地（図\ref{fig: rough})，非整備の草地(図\ref{fig: rough2})などの不整地環境である．また，予測の有効性を確認するため，整備環境（図\ref{fig: grass}）でも実験を行った．
画像取得は移動速度0.2m/sで走行させた．また,加速度センサは 100Hz ,カメラは 30fps ,自己位置推定は 3.5Hz で測定を行った.

\begin{figure}[tbp]
	\begin{tabular}{cc}
		\begin{minipage}[b]{0.45\linewidth}
			\centering
			\includegraphics[width=\columnwidth]{fig/gravel.png}
			\subcaption{Gravel}
			\label{fig: gravel}
		\end{minipage} &
		\hspace{0.04\columnwidth}
		\begin{minipage}[b]{0.45\linewidth}
			\centering
			\includegraphics[width=\columnwidth]{fig/rough.png}
			\subcaption{Rough terrain}
			\label{fig: rough}
		\end{minipage} \\

		\begin{minipage}[b]{0.45\linewidth}
				\centering
				\includegraphics[width=\columnwidth]{fig/rough2.png}
				\subcaption{Grass without maintenance}
				\label{fig: rough2}
			\end{minipage} &
			\hspace{0.04\columnwidth}
			\begin{minipage}[b]{0.45\linewidth}
				\centering
				\includegraphics[width=\columnwidth]{fig/grass.png}
				\subcaption{Grass}
				\label{fig: grass}
			\end{minipage}
	\end{tabular}
	\caption{Experimental environment}
\end{figure}

%------------------------------

\subsection{速度一定下での予測結果}

加速度取得時の移動速度を0.5m/sで一定としたときの予測結果を図\ref{fig: result const}に示す．このとき，訓練データ数と検証データ数はそれぞれ500個，141個である．図\ref{fig: result const}の横軸は検証データのサンプル番号，縦軸はコストの値である．
図\ref{fig: result const}より，走行コストの値が0$\sim$5付近の低・中コスト領域では精度よく予測できていることが確認できる．また，地形による振動には地形の粗さによる平均的な振動と石や木の根などの局所的な段差による局所振動がある．本結果では局所的な振動は予測ができない場合があるものの，多くが予測できていることが確認できる．地形の分類手法では，地形の危険性はラベルごとに与えるため平均的な振動のみしか予測できないが，本研究の手法を用いることで地形が類似していても，地面の起伏や進行予定路の障害物の有無などの違いから局所的な振動を予測できることを示した．
しかし，高コスト領域の予測ができていないことが確認できる．高コストデータは振動が大きく危険な地形であるため，本手法ではデータ収集の際にロボットに危険を経験させる必要がある．そのため，ロボットの破損危険性を考慮するとデータの大量収集が困難である．以上から，単純に高コストのデータを増やす以外の適応的な学習手法が必要であり，移動速度変化を考慮した予測手法を提案した．

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=62mm]{fig/result_const.pdf}
		\caption{Prediction results of traversability cost at constant speed (Top: left, Bottom: right)}
		\label{fig: result const}
	\end{center}
\end{figure}

%\begin{table}[ht]
% \caption{Terrain information of validation data}
% \label{tab: valid-terrain}
% \centering
% \footnotesize
% \begin{tabular}{|c|c|}
%  \hline
%  サンプル番号  &地形 \\\hline
%  　1 $\sim$ 38　　&荒れ地 \\\hline
%  39 $\sim$ 73  &非整備の草地 \\\hline
%  74 $\sim$ 105  &芝 \\\hline
%  16 $\sim$ 141  &砂利 \\\hline
  %\hline
% \end{tabular}
%\end{table}

%------------------------------

\subsection{移動速度変化を考慮した予測結果}

移動速度変化を考慮した予測手法の予測結果を図\ref{fig: result safety}に示す．ただし，加速度取得時の速度は0.2m/s, 0.5m/sの2パターンで行った．
図\ref{fig: result safety}の青線は検証画像の実測値であり，赤線は検証画像の移動速度を$0.2,0.3,0.4,0.5 \rm{m/s}$としたときの予測値である．また，黒線の上がり具合は速度依存の傾向に相当しており，速度方向の汎化が確認できる．
図\ref{fig: result safety}より，高コストデータでは移動速度が大きくなるほど，走行コストは単調増加で大きくなり，一方，低コストデータでは低速と高速で走行コストの値に大きな変化はないことが分かる．

本手法の発展として，低・中コストの低速・高速データ，および高コストの低速データを訓練データとして学習することで，移動速度と走行コストの関係を獲得し，速度の汎化作用を利用して最終的に高速の高コストデータの予測が可能となることを期待する．また，高速では踏破が不可能な荒れ地や大きな障害物がある地形でも，低速走行によりデータの収集が可能ならば，予測が可能であると期待される．
これにより，危険地形でのデータ収集の際に，ロボットに生じる振動を抑えることが可能となり，安全性を考慮した地形の危険性予測が可能である．
今後はより速度の汎化精度を高めるために，様々な速度で実験を行い学習データを増やす必要がある．また，速度汎化の評価が必要である．

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=75mm]{fig/result_safety.pdf}
		\caption{Prediction results of running costs considering speed change (Left)}
		\label{fig: result safety}
	\end{center}
\end{figure}

%===========================

\section{結言}

本稿では，不整地環境における移動ロボットの地形危険性予測を行った．移動ロボットにRGB-Dカメラと加速度センサを搭載し，撮影した地形の危険性をロボット自身が経験する自己教師あり学習を用いた予測手法を提案した．また，実環境での実験により振動特徴の予測を行うことで従来手法と比べて手作業を大きく削減し，一部の局所振動の予測にも成功した．しかし，高コスト域の予測は現段階では困難である．さらに，移動速度変化を考慮した自己教師あり学習による地形危険性予測手法を提案し，速度方向の補外効果を持つ予測器の作成に成功した．これにより，危険地形の安全な危険性予測が可能となることが期待される．

今後の展望として，様々な速度で実験を行うことで，速度方向の汎化の精度向上や評価を行う．また，加速度センサだけでなく，音情報などを利用したマルチモーダルな学習を行うことで，今よりもさらに多様な環境での適用を目指す．さらに，これらの予測手法を利用した移動ロボットのナビゲーションを行う．

%===========================

\footnotesize
\begin{thebibliography}{99}

\bibitem{ProbabilityRobotics}
Thrun, S., Burgard, W. and Fox, D., ``Probabilistic Robotics," MIT Press， 2005．

\bibitem{rtabmap}
Labbé, M. and Michaud, F., ``RTAB-Map as an Open-Source Lidar and Visual SLAM Library for Large-Scale and Long-Term Online Operation," Journal of Field Robotics， vol.36， no.2， pp.416--446， 2019．

\bibitem{Valada2016}
Valada, A., Oliveira, G. and Burgard, W., ``Towards robust semantic segmentation using deep fusion," Proc. of Robotics Science and Systems， 2016．

\bibitem{Brooks2012}
Brooks, C. and Iagnemma, K., ``Self-supervised terrain classification for planetary surface exploration rovers," Journal of Field Robotics， vol.29， no.3, pp.445--468， 2012．

\bibitem{Bekhti2020} 
Bekhti, M., Kobayashi, Y., ``Regressed Terrain Traversability Cost for Autonomous Navigation Based on Image Textures," Appl. Sci， vol.10, pp.1195--1210， 2020．

\end{thebibliography}

\normalsize
\end{document}
